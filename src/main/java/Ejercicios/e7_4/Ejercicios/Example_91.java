/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package e7_4.Ejercicios;

import java.util.Scanner;

/**
 *
 * @author krodr
 */
public class Example_91 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] A = new int[n];
        //int[] A = {5, 6, 7, 13, 15, 17, 43, 45, 47, 83};
        for (int i =0 ; i < A.length ; i++) {
            A[i] = input.nextInt();
        }
        int[] B = new int[A.length];
        int index = 0;
        for (int i = 0; i < A.length; i++) {
            int contador = 0;
            for (int j = 2; j < A[i]; j++) {
                if (A[i]%j==0) {
                    contador++;
                    break;
                }
            }
            if (contador==0) {
                B[index] = A[i];
                index++;
            }
        }
        for (int i = 0; i < A.length; i++) {
            int contador = 0;
            for (int j = 2; j < A[i]; j++) {
                if (A[i]%j==0) {
                    contador++;
                    break;
                }
            }
            if (contador != 0) {
                B[index] = A[i];
                index++;
            }
        }
        for (int i = 0; i < B.length; i++) {
            System.out.print(B[i] + " ");
        }
    }
}
