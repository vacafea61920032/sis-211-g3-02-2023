/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TareasSolucionario.Tarea_2;

import TareasSolucionario.Tarea_2.Ejercicio3.AccountHolder;
import TareasSolucionario.Tarea_2.Ejercicio3.BankAccount;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author PERSONAL
 */
public class Ejercicio_3 {

    public static void main(String[] args) {
        //AccountHolder accountHolder = new AccountHolder("Kevin", "456789", "Calle la paz #4", "67890345", "alguien@gmail.com");

        //System.out.println(accountHolder);
        //System.out.println(accountHolder.getName());
        // cambiamos el nombre
        //accountHolder.setName("Alexis");
        //System.out.println(accountHolder.getName());
        ArrayList<BankAccount> bankAccounts = new ArrayList<>();
        Scanner x = new Scanner(System.in);
        int opcion;
        do {

            System.out.println("1.- Crear cuenta\n 2.-Depositar\n 3.- Retirar\n 4.-Consultar Saldo");
            System.out.println("Selecciona una opcion:");
            opcion = x.nextInt();
            switch (opcion) {
                case 1: {
                    System.out.println("Ingrese su nombre:");
                    String name = x.next();
                    System.out.println("Ingrese su ci:");
                    String ci = x.next();
                    System.out.println("Ingrese su direccion:");
                    String address = x.next();

                    System.out.println("Ingrese su numero de telefono:");
                    String phone = x.next();

                    System.out.println("Ingrese su correo electronico:");
                    String email = x.next();
                    AccountHolder accountHolder = new AccountHolder(
                            name,
                            ci,
                            address,
                            phone,
                            email
                    );
                    System.out.println("Datos para la cuenta del banco");
                    System.out.println("Ingrese un saldo inicial para su cuenta:");
                    Double saldo = x.nextDouble();
                    BankAccount bankAccount = new BankAccount(saldo, accountHolder);
                    // añadimos la cuenta dle banco en el array dinamico
                    bankAccounts.add(bankAccount);
                    break;
                }
                case 2: {
                    String ci = x.next();

                    for (BankAccount bankAccount : bankAccounts) {
                        if (bankAccount.getAccountHolder().getCi().equals(ci)) {
                            Double amount;
                            do {
                                amount = x.nextDouble();
                                if (amount < 10) {
                                    System.out.println("No se puede depositar una cantidad menor a 10bs");
                                } else {
                                    bankAccount.deposit(amount);
                                    System.out.println("Su deposito a sido afectuado su saldo es" + bankAccount.queryBalance());
                                }
                            } while (amount < 10);
                        }
                    }
                    break;
                }

                default:
                    throw new AssertionError();
            }
        } while (opcion != 0);

    }
}
