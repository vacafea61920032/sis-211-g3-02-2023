
package Clase_14_octubre.Ejemplo_1;

import java.io.ObjectInputStream;
import java.util.Scanner;


public class Point {
   
    Integer x;
    Integer y;
    
    
    
    public Point(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }
    
    // get -> obtener
    // set -> asignar / cambiar
    
    public Integer getX(){
        return this.x;
    } 
    
    public void setX(Integer x){
        this.x = x;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY(Integer y){
        this.y = y;
    }
    
    public String getData(){
        return "X = " +this.getX() + "\nY =" + this.getY();
    }
    
    public Point sumaPuntos(Point a, Point b){
        int sumaX = a.getX() + b.getX();
        int sumaY = a.getY() + b.getY();
        
        return new Point(sumaX, sumaY);
    }
    
    public static Point sumaPuntos(Point ...points){
        int sumaX = 0;
        int sumaY = 0;
        for(Point point : points){
            sumaX += point.getX(); // sumaX = sumaX + point.getX()
            sumaY += point.getY();
        }
        return new Point(sumaX, sumaY);
    }
    
}
