/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clase_16_septiembre_2023;

/**
 *
 * @author PERSONAL
 */
public class N_1_Condicionales {
    public static void main(String[] args) {
        // Las condiconales son estructuras que permiten al algoritmo poder tomar deciciones en tiempo de ejecucion, es decir una parte del codigo solo se ejecutara si esque cierta condicion llega a cumplirse
        condicionalIf();
    }
    
    public static void condicionalIf(){
        /**
         * Este tipo de condicion
         */
        
        // declramos un variable de tipo int
        int number = 10;
        // lo habitual es cada vez que se utiliza un if, y la condicon que valida el if no se cumple y es hay donde entra la condicional else
        // em el ejemplo acontinuacion validamos nsi un numero es par o impar. Podemos intrepetralo de la siguiente manera: Si el numero es par imprime por consola que el numero es par en caso contrario imprime por consola que el numero es impar
        
        if (number % 2 == 0) {
            System.out.println("El numero " + number + " es par");
        }else{
            System.out.println("El numero " + number + " es impar");
        }
    }
}
