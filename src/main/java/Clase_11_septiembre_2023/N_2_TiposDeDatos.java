/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clase_11_septiembre_2023;

/**
 *
 * @author PERSONAL
 */
public class N_2_TiposDeDatos {
    public static void main(String[] args) {
        // byte, short, int, long, float, double, char, boolean, cadenas(string)
        
        // byte -> -128 a 127
        byte b = 127;
        
        // short -> -32768 a 32767
        short s = 32767;
        
        // int -> -2,147,483,648 a 2,147,483,647 
        int i = 2147483647;
        
        // long -> -9,223,372,036,854,775,808  a 9223372036854775807
        long l = Long.MIN_VALUE;
        //System.out.println(l);
        
        // float -> -3.4028235x10^38 a 3.4028235x10^38
        float max = Float.MAX_VALUE;
        float min = Float.MIN_VALUE;
        //System.out.println(min +" a " + max);
        
        
        // double -> 4.9E^-324 a 1.7976931348623157E^308
        double d = 1.7976931348623157;
        
        
        // char 
        char c = 'h';
        
        
        // cadenas(string)
        String cadena = "esto es un string";
        
        
        // boolean -> true | false
        boolean isTrue = true;
        
        
        // java es de tipo case sensitive (distingue entre mayusculas y minusculas)
        // isTrue no es igual IsTrue
    }
}
