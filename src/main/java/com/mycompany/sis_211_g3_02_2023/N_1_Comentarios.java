/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.sis_211_g3_02_2023;

import javax.xml.transform.sax.SAXSource;

/**
 *
 * @author PERSONAL
 */
public class N_1_Comentarios {

    public static void main(String[] args) {

        // tipos de comentarios
        // comentarios en linea
        System.out.println("comentarios en linea");
        int numero = 10; // aqui se declara una variable
        System.out.println(numero); // aqui se imprime por consola la variable numero

        System.out.println("====================================");
        // comentarios en bloque
        System.out.println("comentarios en linea");

        /*int numero2 = 20;
        for (int i = 0; i < numero2; i++) {
            System.out.println(i);
        }*/
        System.out.println("====================================");
        // comentarios en bloque
        System.out.println("comentarios de documentacion");
        generarComentario.sumarNumeros(10, 10);
        Math.max(10,20);
    }
    
    /*
    5 puntos Mayerly Belen Coro
    */
}

/**
 * @author : autor
 * @Descripcion: que hace esta clase
 */
class generarComentario {

    /**
     * @parametros: int a, int b
     * @return int
     */
    public static int sumarNumeros(int a, int b) {
        return a + b;
    }
}
